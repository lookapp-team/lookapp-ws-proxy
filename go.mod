module proxy

go 1.14

require (
	github.com/apache/pulsar/pulsar-client-go v0.0.0-20201107015643-db6afd528411
	github.com/gobwas/httphead v0.0.0-20200921212729-da3d93bc3c58 // indirect
	github.com/gobwas/pool v0.2.1 // indirect
	github.com/gobwas/ws v1.0.4
	github.com/mailru/easygo v0.0.0-20190618140210-3c14a0dc985f
	golang.org/x/sys v0.0.0-20201107080550-4d91cf3a1aaf // indirect
)
