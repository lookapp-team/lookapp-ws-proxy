package main

import (
	"log"
	"time"

	"github.com/apache/pulsar/pulsar-client-go/pulsar"
)

func main() {

	cluster, err := pulsar.NewClient(pulsar.ClientOptions{
		URL: "pulsar://broker1.openmvc.ru:6650",
		// MessageListenerThreads: runtime.NumCPU(),
		// OperationTimeoutSeconds: 5,
	})
	if err != nil {
		log.Fatalf("pulsar client error: %v", err)
	}

	service := NewWebSocketService(":3456", 128, 1, 128, time.Millisecond*1, time.Millisecond*1000, &cluster)

	service.name = "dev" //os.Getpid()

	service.Start()

	select {}
}
