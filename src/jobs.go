package main

import (
	"container/list"
	"fmt"
	"sync"
	"time"
)

// ErrScheduleTimeout returned by Pool to indicate that there no free
// goroutines during some period of time.
var ErrScheduleTimeout = fmt.Errorf("schedule error: timed out")

type Job struct {
	task func()
	wait chan struct{}
}

func NewJob(task func()) *Job {
	j := &Job{
		task: task,
	}
	return j
}

type Worker struct {
	io     chan struct{}
	run    chan struct{}
	active *Job
	queue  *list.List
	mu     sync.RWMutex
}

func NewWorker() *Worker {
	w := &Worker{
		io:     make(chan struct{}, 1),
		run:    make(chan struct{}, 1),
		queue:  list.New(),
		active: nil,
	}
	return w
}

func (w *Worker) Run() {
	select {
	case w.run <- struct{}{}:
	default:
	}
}

func (w *Worker) do() {
	for {
		w.mu.RLock()
		j := w.queue.Front()
		w.mu.RUnlock()

		if j == nil || j.Value == nil {
			w.mu.Lock()
			w.active = nil
			w.mu.Unlock()
			<-w.run
			continue
		}

		w.mu.Lock()
		w.active = j.Value.(*Job)
		w.queue.Remove(j)
		w.mu.Unlock()

		job := (*w.active)
		job.task()

		if job.wait != nil {
			select {
			case job.wait <- struct{}{}:
			default:
			}
		}

		select {
		case w.io <- struct{}{}:
		default:
		}

		// select {
		// case <-time.After(time.Millisecond):
		// }
	}
}

func (w *Worker) PushFront(job *Job) *list.Element {
	w.mu.Lock()
	el := w.queue.PushFront(job)
	active := w.active != nil
	w.mu.Unlock()
	if !active {
		w.Run()
	}
	return el
}

func (w *Worker) PushBack(job *Job) *list.Element {
	w.mu.Lock()
	el := w.queue.PushBack(job)
	active := w.active != nil
	w.mu.Unlock()
	if !active {
		w.Run()
	}
	return el
}

func (w *Worker) Remove(element *list.Element) *Job {
	w.mu.Lock()
	j := w.queue.Remove(element).(*Job)
	w.mu.Unlock()
	return j
}

func (w *Worker) Len() int {
	w.mu.RLock()
	n := w.queue.Len()
	w.mu.RUnlock()
	return n
}

type WorkersPool struct {
	workers    []*Worker
	nextWorker int
}

func NewWorkersPool(size, queue, spawn int) *WorkersPool {
	if spawn <= 0 && queue > 0 {
		panic("dead queue configuration detected")
	}
	if spawn > size {
		panic("spawn > workers")
	}

	p := &WorkersPool{
		workers:    make([]*Worker, 0),
		nextWorker: 0,
	}

	for i := 0; i < spawn; i++ {
		w := NewWorker()
		p.workers = append(p.workers, w)
		go w.do()
	}

	return p
}

func (p *WorkersPool) PreferredWorker() *Worker {
	n := 0
	min := p.workers[n].Len()
	for i := 1; i < len(p.workers); i++ {
		u := p.workers[i].Len()
		if u < min {
			min = u
			n = i
		}
	}
	return p.workers[n]
}

func (p *WorkersPool) Job(task func()) {
	w := p.PreferredWorker()
	w.PushBack(NewJob(task))
}

func (p *WorkersPool) PriorityJob(task func()) {
	w := p.PreferredWorker()
	w.PushFront(NewJob(task))
}

func (p *WorkersPool) Schedule(task func()) {
	j := NewJob(task)
	j.wait = make(chan struct{}, 1)
	w := p.PreferredWorker()
	w.PushFront(j)
	<-j.wait
}

func (p *WorkersPool) ScheduleTimeout(timeout time.Duration, task func()) error {
	j := NewJob(task)
	j.wait = make(chan struct{}, 1)

	w := p.PreferredWorker()

	if w.Len() == 0 {
		w.PushFront(j)
		<-j.wait
		return nil
	}

	select {
	case <-time.After(timeout):
		return ErrScheduleTimeout
	case <-w.io:
		w.PushFront(j)
	}
	<-j.wait
	return nil
}
