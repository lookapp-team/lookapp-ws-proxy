package main

type Object map[string]interface{}

type Headers map[string]string

type Request struct {
	ID     int    `json:"id,omitempty"`
	Method string `json:"method,omitempty"`
	Topic  string `json:"topic,omitempty"`
	Params Object `json:"params, omitempty"`
}

type Response struct {
	ID     int    `json:"id,omitempty"`
	Result Object `json:"result,omitempty"`
}

type Error struct {
	ID    int    `json:"id,omitempty"`
	Error Object `json:"error,omitempty"`
}

type Session struct {
	ID     string
	Invite string
	Name   string
	Flag   bool
	Scores int
	Pos    int
}
