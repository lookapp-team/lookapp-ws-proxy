package main

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net"
	"sync"
	"time"

	"github.com/gobwas/ws"
	"github.com/gobwas/ws/wsutil"

	"github.com/apache/pulsar/pulsar-client-go/pulsar"
)

var (
	methodValidate = "validate"
	methodRename   = "rename"
)

// User represents user connection.
// It contains logic of receiving and sending messages.
// Application should call Receive() to read user's incoming message.
type User struct {
	io   sync.Mutex
	conn io.ReadWriteCloser

	id       uint
	endpoint string
	new      bool
	time     time.Time
	changed  bool
	chat     *Chat
	close    bool

	en sync.RWMutex
	// methods []string

	// queue chan pulsar.ProducerMessage
}

// Receive reads next message from user's underlying connection.
// It blocks until full message received.
func (u *User) Receive() error {

	buf, err := u.readBuffer()

	if err != nil {
		if err, ok := err.(net.Error); !ok || !err.Timeout() {
			u.conn.Close()
			return err
		}
	}

	if buf == nil || len(*buf) == 0 {
		return err
	}

	// defer func() {
	// 	buf = nil
	// }()

	log.Printf("pass message from %s %s", u.endpoint, string(*buf))

	err = (*u.chat.routeTopic).Send(context.Background(), pulsar.ProducerMessage{
		Payload: *buf,
		Properties: map[string]string{
			"endpoint": u.endpoint,
			"proxy":    u.chat.proxyTopic,
		},
	})
	if err != nil {
		log.Fatalf("pulsar send error %v", err)
	}

	return nil
}

// readRequests reads raw buffer from connection.
// It takes io mutex.
func (u *User) readBuffer() (*[]byte, error) {
	u.io.Lock()
	defer u.io.Unlock()

	h, r, err := wsutil.NextReader(u.conn, ws.StateServerSide)
	if err != nil {
		return nil, err
	}
	if h.OpCode.IsControl() {
		return nil, wsutil.ControlFrameHandler(u.conn, ws.StateServerSide)(h, r)
	}

	const minRead = 512
	buf := make([]byte, 0, minRead)
	for {
		if cap(buf)-len(buf) < minRead {
			newBuf := make([]byte, len(buf), 2*cap(buf)+minRead)
			copy(newBuf, buf)
			buf = newBuf
		}

		n, err := r.Read(buf[len(buf):cap(buf)])

		buf = buf[0 : len(buf)+n]

		if err != nil {
			if err == io.EOF {
				break
			}
			return nil, err
		}
	}

	return &buf, nil
}

// readRequests reads json-rpc request from connection.
// It takes io mutex.
func (u *User) readRequest() (*Object, error) {
	u.io.Lock()
	defer u.io.Unlock()

	h, r, err := wsutil.NextReader(u.conn, ws.StateServerSide)
	if err != nil {
		return nil, err
	}
	if h.OpCode.IsControl() {
		return nil, wsutil.ControlFrameHandler(u.conn, ws.StateServerSide)(h, r)
	}

	msg := &Object{}
	decoder := json.NewDecoder(r)
	if err := decoder.Decode(msg); err != nil {
		log.Printf("websocket decode error %s %v", u.endpoint, err)
		return nil, nil
	}

	return msg, nil
}

func (u *User) writeErrorTo(req *Request, err Object) error {
	return u.write(Error{
		ID:    req.ID,
		Error: err,
	})
}

func (u *User) writeResultTo(req *Request, result Object) error {
	return u.write(Response{
		ID:     req.ID,
		Result: result,
	})
}

func (u *User) writeNotice(method string, params Object) error {
	return u.write(Request{
		Method: method,
		Params: params,
	})
}

func (u *User) writeMessage(buf []byte) error {
	w := wsutil.NewWriter(u.conn, ws.StateServerSide, ws.OpText)

	u.io.Lock()
	defer u.io.Unlock()

	w.Write(buf)

	return w.Flush()
}

func (u *User) write(x interface{}) error {
	w := wsutil.NewWriter(u.conn, ws.StateServerSide, ws.OpText)
	encoder := json.NewEncoder(w)

	u.io.Lock()
	defer u.io.Unlock()

	if err := encoder.Encode(x); err != nil {
		return err
	}

	return w.Flush()
}

func (u *User) writeRaw(p []byte) error {
	u.io.Lock()
	defer u.io.Unlock()

	_, err := u.conn.Write(p)

	return err
}

func (u *User) messageBuffer(x interface{}) ([]byte, error) {
	var buf bytes.Buffer

	w := wsutil.NewWriter(&buf, ws.StateServerSide, ws.OpText)
	encoder := json.NewEncoder(w)

	u.io.Lock()
	defer u.io.Unlock()

	if err := encoder.Encode(x); err != nil {
		return nil, err
	}

	if err := w.Flush(); err != nil {
		return nil, err
	}

	return buf.Bytes(), nil
}

// Chat contains logic of user interaction.
type Chat struct {
	mu  sync.RWMutex
	ns  map[string]*User
	seq uint

	pool *WorkersPool
	out  chan []byte

	bus   chan pulsar.ProducerMessage
	route chan pulsar.ProducerMessage

	proxyTopic string
	routeTopic *pulsar.Producer

	cv *Request
}

// NewChat creates chat to route user's WebSocket messages to the Pulsar topics
func NewChat(pool *WorkersPool) *Chat {
	chat := &Chat{
		pool:  pool,
		ns:    make(map[string]*User),
		out:   make(chan []byte, 1),
		bus:   make(chan pulsar.ProducerMessage),
		route: make(chan pulsar.ProducerMessage),
	}

	go chat.writer()

	return chat
}

// Register registers new connection as a User.
func (c *Chat) Register(conn net.Conn) *User {
	user := &User{
		chat:  c,
		conn:  conn,
		close: false,
	}

	c.mu.Lock()
	{
		user.id = c.seq
		user.endpoint = conn.RemoteAddr().String()
		c.ns[user.endpoint] = user
		c.seq++
	}
	c.mu.Unlock()

	return user
}

// Remove removes user from chat.
func (c *Chat) Remove(user *User) {

	(*c.routeTopic).Send(context.Background(), pulsar.ProducerMessage{
		Payload: []byte(fmt.Sprintf("{\"method\":\"disable\"}")),
		Properties: map[string]string{
			"endpoint": user.endpoint,
		},
	})

	c.mu.Lock()
	removed := c.remove(user)
	c.mu.Unlock()

	if !removed {
		return
	}
}

// Broadcast sends message to all alive users.
func (c *Chat) Broadcast(method string, params Object) error {
	var buf bytes.Buffer

	w := wsutil.NewWriter(&buf, ws.StateServerSide, ws.OpText)
	encoder := json.NewEncoder(w)

	r := Request{Method: method, Params: params}
	if err := encoder.Encode(r); err != nil {
		return err
	}
	if err := w.Flush(); err != nil {
		return err
	}

	c.out <- buf.Bytes()

	return nil
}

// BroadcastMessage sends message to all alive users.
func (c *Chat) BroadcastMessage(data []byte) error {
	var buf bytes.Buffer

	w := wsutil.NewWriter(&buf, ws.StateServerSide, ws.OpText)

	w.Write(data)
	if err := w.Flush(); err != nil {
		return err
	}

	c.out <- buf.Bytes()

	return nil
}

// writer writes broadcast messages from chat.out channel.
func (c *Chat) writer() {
	for bts := range c.out {
		c.mu.RLock()
		ns := c.ns
		c.mu.RUnlock()

		for _, u := range ns {
			u := u // For closure.
			c.pool.Job(func() {
				u.writeRaw(bts)
			})
		}
	}
}

// mutex must be held.
func (c *Chat) remove(user *User) bool {
	if _, has := c.ns[user.endpoint]; has {
		delete(c.ns, user.endpoint)
		return true
	}
	return false
}

func timestamp() int64 {
	return time.Now().UnixNano() / int64(time.Millisecond)
}
