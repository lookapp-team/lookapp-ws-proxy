package main

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net"
	"os"
	"time"

	"github.com/apache/pulsar/pulsar-client-go/pulsar"

	WebSocket "github.com/gobwas/ws"
	"github.com/mailru/easygo/netpoll"
)

// WebSocketService represents WebSocket server.
// It contains logic of accepting HTTP connection and upgrading them to the WebSocket connection.
// Application should call Start to accept user's incoming connections.
type WebSocketService struct {
	poller       *netpoll.Poller
	pool         *WorkersPool
	listener     *net.Listener
	acceptDesc   *netpoll.Desc
	accept       chan error
	chat         *Chat
	readTimeout  time.Duration
	writeTimeout time.Duration

	name       string
	cluster    *pulsar.Client
	topics     map[string]pulsar.Consumer
	feed       chan pulsar.ConsumerMessage
	routeTopic pulsar.Producer
	busTopic   pulsar.Producer
}

// NewWebSocketService create instance of WebSocketService
func NewWebSocketService(listenAddress string, workers int, queue int, spawn int, readTimeout time.Duration, writeTimeout time.Duration, cluster *pulsar.Client) *WebSocketService {
	poller, err := netpoll.New(nil)

	if err != nil {
		log.Fatal(err)
	}

	ln, err := net.Listen("tcp", listenAddress)
	if err != nil {
		log.Fatal(err)
	}

	log.Printf("websocket is listening on %s", ln.Addr().String())

	pool := NewWorkersPool(workers, queue, spawn)

	ws := &WebSocketService{
		poller:       &poller,
		listener:     &ln,
		accept:       make(chan error, 1),
		acceptDesc:   netpoll.Must(netpoll.HandleListener(ln, netpoll.EventRead|netpoll.EventOneShot)),
		pool:         pool,
		chat:         NewChat(pool),
		readTimeout:  readTimeout,
		writeTimeout: writeTimeout,
		topics:       make(map[string]pulsar.Consumer),
		feed:         make(chan pulsar.ConsumerMessage, 1),
		cluster:      cluster,
		name:         "dev",
	}

	ws.makeSubscriptions()

	go ws.feeding()

	go ws.routing()

	go ws.bus()

	return ws
}

// Subscribe to the topic with specified options
func (ws *WebSocketService) Subscribe(name string, options pulsar.ConsumerOptions) pulsar.Consumer {
	if len(options.Topic) == 0 {
		options.Topic = name
	}
	topic, err := (*ws.cluster).Subscribe(options)
	if err != nil {
		log.Fatalf("pulsar subscribe error: %v", err)
		return nil
	}
	ws.topics[name] = topic
	return topic
}

// Start acception of user's incoming connections
func (ws *WebSocketService) Start() {
	(*ws.poller).Start(ws.acceptDesc, ws.accepting)
}

func (ws *WebSocketService) accepting(e netpoll.Event) {
	err := ws.pool.ScheduleTimeout(time.Millisecond*500, func() {
		conn, err := (*ws.listener).Accept()
		if err != nil {
			ws.accept <- err
			return
		}
		ws.connecting(conn)
		ws.accept <- nil
	})

	if err == nil {
		err = <-ws.accept
	}

	if err != nil {
		if err == ErrScheduleTimeout {
			goto cooldown
		}
		if ne, ok := err.(net.Error); ok && ne.Temporary() {
			goto cooldown
		}

		log.Fatalf("accept error: %v", err)

	cooldown:
		delay := 5 * time.Millisecond
		log.Printf("accept error: %v; retrying in %s", err, delay)
		time.Sleep(delay)
	}

	(*ws.poller).Resume(ws.acceptDesc)
}

func (ws *WebSocketService) connecting(conn net.Conn) {

	log.Printf("websocket open %s", conn.RemoteAddr().String())

	safeConn := deadliner{conn, ws.readTimeout, ws.writeTimeout}

	_, err := WebSocket.DefaultUpgrader.Upgrade(conn)
	if err != nil {
		log.Printf("%s: upgrade error: %v", conn.LocalAddr().String()+" > "+conn.RemoteAddr().String(), err)
		conn.Close()
		return
	}

	user := (*ws.chat).Register(safeConn)

	desc := netpoll.Must(netpoll.HandleRead(conn))

	(*ws.poller).Start(desc, func(ev netpoll.Event) {
		if ev&(netpoll.EventReadHup|netpoll.EventHup) != 0 && !user.close {
			user.close = true
			(*ws.poller).Stop(desc)
			(*ws.pool).Job(func() {
				log.Printf("websocket close %s", user.endpoint)
				(*ws.chat).Remove(user)
			})
			return
		}
		if user.close {
			return
		}
		(*ws.pool).PriorityJob(func() {
			if user.close {
				return
			}
			var err error = nil
			for err == nil {
				err = user.Receive()
			}
			if e, ok := err.(net.Error); err != nil && (!ok || !e.Timeout()) {
				if user.close {
					return
				}
				log.Printf("websocket receive error %s %v", user.endpoint, e)
				user.close = true
				(*ws.poller).Stop(desc)
				(*ws.chat).Remove(user)
			}
		})
	})

}

func (ws *WebSocketService) feeding() {
	for msg := range ws.feed {
		ws.feedMessage(msg)
	}
}

func (ws *WebSocketService) feedMessage(msg pulsar.ConsumerMessage) {
	payload := msg.Payload()
	props := msg.Properties()

	// fmt.Println(string(payload), props)

	msg.AckID(msg.ID())

	if _, has := props["endpoint"]; has {
		ws.chat.mu.RLock()
		ns := ws.chat.ns
		ws.chat.mu.RUnlock()

		if _, has := ns[props["endpoint"]]; has {
			u, _ := ns[props["endpoint"]]
			u.chat.pool.PriorityJob(func() {
				// log.Printf("got message from %s %d bytes", u.endpoint, len(payload))
				log.Printf("route message to %s %s", u.endpoint, string(payload))
				u.writeMessage(payload)
			})
			return
		}

		if props["endpoint"] != "*" {
			log.Printf("[%s] remote address error %s %v", msg.Message.Topic(), string(payload), props)
			return
		}
	}

	req := &Request{}
	if err := json.Unmarshal(payload, req); err != nil {
		log.Printf("[%s] unxpected broadcast message format %s %v", msg.Message.Topic(), string(payload), props)
		return
	}

	switch req.Method {
	default:
		ws.chat.BroadcastMessage(payload)
	}
}

func (ws *WebSocketService) makeSubscriptions() {
	ws.Subscribe("proxy", pulsar.ConsumerOptions{
		Topic:               fmt.Sprintf("persistent://lookapp/dev/proxy-%s", ws.name),
		SubscriptionName:    fmt.Sprintf("proxy"),
		Type:                pulsar.Exclusive,
		SubscriptionInitPos: pulsar.Latest,
		MessageChannel:      ws.feed,
		// ReceiverQueueSize:   1,
	})

	ws.Subscribe("feed", pulsar.ConsumerOptions{
		Topic:               fmt.Sprintf("persistent://lookapp/dev/feed"),
		SubscriptionName:    fmt.Sprintf("proxy-%d", os.Getpid()),
		Type:                pulsar.Exclusive,
		SubscriptionInitPos: pulsar.Earliest,
		//MessageChannel:      ws.feed,
		// ReceiverQueueSize:   1,
	})

	busTopic, err := (*ws.cluster).CreateProducer(pulsar.ProducerOptions{
		Topic: fmt.Sprintf("persistent://lookapp/dev/bus"),
	})
	ws.busTopic = busTopic
	if err != nil {
		log.Fatalf("pulsar producer error: %v", err)
	}

	routeTopic, err := (*ws.cluster).CreateProducer(pulsar.ProducerOptions{
		Topic:              fmt.Sprintf("persistent://lookapp/dev/route"),
		MessageRoutingMode: pulsar.RoundRobinDistribution,
		BlockIfQueueFull:   false,
	})
	ws.routeTopic = routeTopic
	ws.chat.routeTopic = &routeTopic
	ws.chat.proxyTopic = ws.topics["proxy"].Topic()
	if err != nil {
		log.Fatalf("pulsar producer error: %v", err)
	}
}

func (ws *WebSocketService) routing() {
	proxyTopic := ws.topics["proxy"].Topic()
	ctx := context.Background()
	for msg := range ws.chat.route {
		msg.Properties["proxy"] = proxyTopic
		err := ws.routeTopic.Send(ctx, msg)
		if err != nil {
			log.Printf("pulsar produce message error %v", err)
		}
	}
}

func (ws *WebSocketService) bus() {
	proxyTopic := ws.topics["proxy"].Topic()
	ctx := context.Background()
	for msg := range ws.chat.bus {
		msg.Properties["proxy"] = proxyTopic
		err := ws.busTopic.Send(ctx, msg)
		if err != nil {
			log.Printf("pulsar produce message error %v", err)
		}
	}
}
