GO_PATH=$(PWD):$(GOPATH)

main:
	make proxy

proxy:
	go build -o ./bin/ws-proxy ./src

run:
	go run ./src